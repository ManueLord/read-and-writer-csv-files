import csv

data = [] 
with open('Libro1.csv', encoding='utf-8') as csvfile:
    spamreader = csv.DictReader(csvfile)
    for sr in spamreader:
        data.append(sr)

with open('test.csv', mode='w', newline='', encoding='utf-8') as csvfile:
    fieldnames = ['OrderDate', 'Region', 'Rep', 'Item', 'Units', 'UnitCost', 'Total']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writeheader()
    for i, e in enumerate(data):
        print(i, e)
        writer.writerow(e)
