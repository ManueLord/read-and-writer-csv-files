import csv

input_file = open('Hoja1.csv', 'r', encoding='utf-8')
output_file = open('test.csv', mode='w', newline='', encoding='utf-8')

data = csv.reader(input_file, delimiter=' ', quotechar='|')
writer = csv.writer(output_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)# dialect='excel')

for line in data:
    d = str(line)
    writer.writerow(d.split(','))

input_file.close()
output_file.close()