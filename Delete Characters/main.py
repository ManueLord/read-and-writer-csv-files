import csv 
import re

data = []

with open('Libro1.csv', encoding='utf-8') as csvfile:
    spamreader = csv.DictReader(csvfile)
    for sr in spamreader:
        # In this check all characters and delete the characters that they aren't allowed
        d = {re.sub(r'[^A-Za-z]+','',k):re.sub(r'[^A-Za-z0-9. ! ]+','',v) for k,v in sr.items()}
        data.append(d)

with open('test.csv', mode='w', newline='', encoding='utf-8') as csvfile:
    fieldnames = ['OrderDate', 'Region', 'Rep', 'Item', 'Units', 'UnitCost', 'Total']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writeheader()
    for e in data:
        writer.writerow(e)
